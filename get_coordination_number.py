#Description: This script calculates coordination number
#of specified atoms in your XYZ file based on cutoff radius 
#-----------------------------------------------------------
# Required Files: valid XYZ file 
# Required Package: ASE and numpy This script was tested with ASE 3.13.0 
# Usage: python get_coordination_number.py 
#
# Output: 
#         Prints out detailed results about coordination number
#          for each specified atom 
#-----------------------------------------------------------
# Author: Satish Kumar Iyemperumal e-mail: satish2414 [at] gmail.com
# Date:   Jan 31, 2018 
#-----------------------------------------------------------

from ase.data import covalent_radii
from ase.neighborlist import NeighborList
from ase.io import read
import numpy as np

fname = raw_input("Enter the XYZ movie or optimized XYZ coordinate file name:\n")
s=read(fname,index=-1)
cutoffs = covalent_radii[s.get_atomic_numbers()]
cutoffs = np.zeros(len(s))

def auto_detect():
    read_cutoff = raw_input("Enter the cutoff radius in Angstroms up to which \
a neighboring atom is considered as coordinated:\n")
    print "WARNING: Two similar atoms can be double counted with this code. If two similar \
atoms are say 4 Anst apart and you choose 2.2 Angst as cutoff radius, then the \
atom 4 Angst apart will be counted as neighbor (as 4 < 4.4). So make sure you manually \
disregard such double double counting." 
    
    atom_indices = [atom.index for atom in s if atom.symbol != 'Ti' \
                      and atom.symbol != 'O' and atom.symbol != 'C']
    print "Detected atoms to be searched for coordination number are \
:\n", atom_indices
    
    for i in atom_indices:
        cutoffs[i] = float(read_cutoff)

    print cutoffs    
    get_coordination_number(cutoffs, atom_indices)

def manual_detect():
    read_cutoff = raw_input("Enter the cutoff radius in Angstroms up to which \
a neighboring atom is considered as coordinated:\n")
    print "WARNING: Two similar atoms can be double counted with this code. If two similar \
atoms are say 4 Anst apart and you choose 2.2 Angst as cutoff radius, then the\
atom 4 Angst apart will be counted as neighbor (as 4 < 4.4). So make sure you manually \
disregard such double double counting." 

    tmp = raw_input("Enter atom indices between 0 to n-1, where n \
is number of atoms in your system) separated by comma. Example: 0, 5, 287:\n")
    tmp1 = tmp.split(",")
    atom_indices = [int(i) for i in tmp1]
    print "Detected atoms to be searched for coordination number are \
:\n", atom_indices

    for i in atom_indices:
        cutoffs[i] = float(read_cutoff)

    print cutoffs    
    get_coordination_number(cutoffs, atom_indices)

def get_coordination_number(cutoffs, atom_indices):
    nl = NeighborList(cutoffs, skin=0.0, self_interaction=False, bothways=True)
    nl.update(s)         
    
    for n, i in enumerate(s):
        if i.index in atom_indices:
            indices, offsets = nl.get_neighbors(n)
            print "---------------------" 
            print "Atom identifier: ", i
            print "Atoms indices detected as neighbors: ", indices
            print "Number of nearest neighbors to this %s-%d = %d" \
                          %(i.symbol,i.index,len(indices))

if __name__ == "__main__": 
    user_input = int(raw_input("Enter 1: for finding the coordination number\
of all atoms other than Ti, O, and C \nEnter 2: for manually entering list of\
atom indices you want to find coordination number for\n"))
    if user_input == 1:
        auto_detect()
    elif user_input == 2:
        manual_detect()
    else:
        print "Wrong value specified. Enter either 1 or 2"
